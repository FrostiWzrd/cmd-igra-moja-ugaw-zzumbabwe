@echo off 

:Menu
cls

type Naslov.txt
echo .
echo 1. Start
echo 2. Nastavitve
echo 3. Izhod
set /p neki= : 
if %neki%==1 goto Zacetek
if %neki%==2 goto Nalozi
if %neki%==3 goto Exit
pause >nul
goto Menu

:Exit
cls
echo Hvala za igranje moje igre!
pause
exit

:Zacetek1
cls
echo ==================
echo  Kako ti je ime?
echo ==================
set /p NAME=
set MHP=35
set HP=%MHP%
set HTR=2
set OBR=0
set DMG=4
set MK=0
set NAP=3
set LOT=0
set OVN=0
set CAR=0
set TRG=1
set eNAME=Harpi
pause >nul
goto carIn

:Zacetek
cls
type Zacetek.txt
set /p odgovor=
if %odgovor%==ja goto Zacetek1
if %odgovor%==ne goto Exit
goto Zacetek

:carIn
cls
if %eNAME%==Kerber goto preveriK
if %HP% GTR %MHP% set HP=%MHP%
if %HP% LEQ 0 set HH=%MHP%
set Spor=Spopad!
type Mozn.txt
echo .
echo         %NAME%
echo ======================                  
echo    Zivljenje:%HP%
echo    Moc:%DMG%
echo    Hitrost: %HTR%
echo    Obramba:%OBR%
echo    Premozenje:%LOT%
echo    St. zmag:%MK%
echo ======================
choice /c 1234567 /n >nul
if %ERRORLEVEL%==1 goto Harpi
if %ERRORLEVEL%==2 goto Preveri1
if %ERRORLEVEL%==3 goto Preveri2
if %ERRORLEVEL%==4 goto Preveri3
if %ERRORLEVEL%==5 goto preveri
if %ERRORLEVEL%==6 goto InvPogled
if %ERRORLEVEL%==7 goto Razvi
goto carIn

:preveri
if %TRG% GEQ 1 ( goto Dialog ) else ( goto Trgovina )

:Trgovina
cls
set Spor=Dobrodosli!
echo ===================
echo      Inventura
echo ===================
echo    Napitek:%NAP%
echo    Car:%CAR%
echo    Olj. venec:%OVN%
echo ===================
echo    Premozenje:%LOT%
echo ===================
echo      Trgovina      
echo ===================                 
echo  1. Napitek:10 
echo  2. Car:20
echo  3. Olj. venec:45                 
echo ===================
echo      4.(nazaj)   
echo ===================
echo :%Spor%
echo ===================
choice /c 1234 /n >nul
if %ERRORLEVEL%==1 set /a spor=Kupil si Napitek & set /a NAP+=1
if %ERRORLEVEL%==2 set /a spor=Kupil si Car & set /a CAR+=1
if %ERRORLEVEL%==3 set /a spor=Kupil si Oljcni venec & set /a OVN+=1
if %ERRORLEVEL%==4 goto carIn
pause>nul
goto Inventura

:Razvi
set MHP=99
set HP=%MHP%
set HTR=99
set OBR=99
set DMG=99
set MK=99
set NAP=99
set LOT=99
goto carIn

:Dialog
cls
type StariG.txt
pause >nul
goto Dialog1

:Dialog1
cls
type StariG1.txt
choice /c 123 /n >nul
if %ERRORLEVEL%==1 set /a DMG+=2
if %ERRORLEVEL%==2 set /a HTR+=2
if %ERRORLEVEL%==3 set /a OBR+=2
pause >nul
goto Dialog2

:Dialog2
cls
type StariG2.txt
choice /c 123 /n >nul
if %ERRORLEVEL%==1 set /a DMG+=2
if %ERRORLEVEL%==2 set /a HTR+=2
if %ERRORLEVEL%==3 set /a OBR+=2
pause >nul
goto Dialog3

:Dialog3
cls
type StariG3.txt
pause >nul
goto nastavi

:nastavi
set /a TRG-=1
goto carIn

:Preveri1
cls
if %MK% GEQ 5 ( goto Gorgon ) else ( goto NsP )
pause

:Preveri2
cls
if %MK% GEQ 10 ( goto Kentaver ) else ( goto NsP )
pause

:Preveri3
cls
if %MK% GEQ 15 ( goto Kerber ) else ( goto NsP )

:preveriK
cls
if %eHP% GEQ 1 goto carIn
set Spor=Premagal si najmocnejso posast %eNAME%, cestitke!
set MHP=99
set HP=%MHP%
set HTR=99
set OBR=99
set DMG=99
set MK=99
set NAP=99
set LOT=99
goto carIn
echo.
echo ===================
echo      Cestitke!
echo ===================                  
echo   Nagrada:+99
echo   St. zmag:+99
echo   Zivljenje:+99
echo   Obramba:+99
echo   Hitrost:+99
echo   Moc:+99
echo ===================
echo :%Spor%
pause >nul
goto carIn


:NsP
cls
echo Nisi Pripravljen!
pause>nul
goto carIn

:Harpi 
cls 
set eNAME=Harpi
set eMHP=20
set eHP=%eMHP%
set eDMG=4
set eLOT=6
goto Bitka

:Gorgon
cls 
set eNAME=Gorgon
set eMHP=40
set eHP=%eMHP%
set eDMG=8
set eLOT=6
goto Bitka

:Kentaver
cls 
set eNAME=Kentaver
set eMHP=50
set eHP=%eMHP%
set eDMG=12
set eLOT=6
goto Bitka

:Kerber
cls 
set eBitkaNAME=Kerber
set eMHP=60
set eHP=%eMHP%
set eDMG=15
set eLOT=6
set BOS=1
goto Bitka

:Bitka
cls
if %HP% GTR %MHP% set HP=%MHP%
if %eHP% LEQ 0 goto Zmaga
echo.
echo ===================
echo       %NAME%
echo ===================                  
echo %HP% / %MHP%
echo Moc:%DMG%
echo Hitrost: %HTR%
echo Obramba:%DEF%
echo ===================
echo.      %NAME%      .    ============
echo....................    1. Napadi
echo         Vs        .    2. Inventura
echo....................    3. Bezi
echo.      %eNAME%     .    ============
echo ===================
echo       %eNAME%
echo ===================
echo %eHP% / %eMHP%
echo Moc:%eDMG%
echo ===================
echo.
echo :%Spor%
choice /c 1234 /n >nul
if %ERRORLEVEL%==1 goto Napad
if %ERRORLEVEL%==2 goto Inventura
if %ERRORLEVEL%==3 goto Bezi
pause>nul

:eBitka
cls
if %HP% GTR %MHP% set HP=%MHP%
if %eHP% LEQ 0 goto Zmaga
if %HP% LEQ 0 goto Zguba
echo.
echo ===================
echo       %NAME%
echo ===================                  
echo %HP% / %MHP%
echo Moc:%DMG%
echo Hitrost: %HTR%
echo Obramba:%OBR%
echo ===================
echo.      %NAME%      .    
echo....................  
echo         Vs        .    
echo....................   
echo.      %eNAME%     .
echo ===================
echo      %eNAME%
echo ===================
echo %eHP% / %eMHP%
echo Moc:%eDMG%
echo ===================
echo.
echo :%Spor%
pause>nul
goto NapadS

:Napad
set /a eHP-=%DMG%
set Spor=Ranil si za %DMG% tock!
goto eBitka

:NapadS
set /a rand=(%RANDOM%*500/32768)+1
if %rand% leq 50 set /a HP-=2*%eDMG% & set Spor=Mocno si bil ranjen! & goto Bitka
set /a HP-=%eDMG%
set /a HP+=%OBR%
set Spor=Ranjen si za %eDMG%
goto Bitka

:Bezi
cls
set Spor=Zbezal si in se poskodoval!
set /a LOT-=%eLOT%
set /a HP-=5
if %MK% GEQ 5 (set /a MK-=4)
if %MK% LEQ 5 (set /a MK-=1)
echo.
echo ===================
echo       ZGUBA!
echo ===================                  
echo   Premozenje:-%eLOT%
echo   St. zmag:-%MK%
echo   Zivljenje:-5
echo ===================
echo :%Spor%
pause >nul
goto carIn

:Inventura
cls
if %NAP% LEQ 0 ( set /a NAP=0 )
echo.
echo ===================
echo      Invntura
echo ===================                  
echo   1. napitek:%NAP%
echo   2. Olj. venec:%OVN%
echo   3. Car:%CAR%
echo ===================
echo      4.(nazaj)   
choice /c 1234 /n >nul
if %ERRORLEVEL%==1 goto Napitek
if %ERRORLEVEL%==2 goto Bitka
if %ERRORLEVEL%==3 goto Bitka
if %ERRORLEVEL%==4 goto Bitka
pause>nul
goto Inventura

:Napitek
if %NAP% LEQ 0 ( goto Nimas ) else ( goto Poz )

:Nimas
set Spor=Primankuje ti napojev!
goto Bitka

:Poz
if %HP% GEQ %MHP% ( set /a HP=%MHP% ) else ( set /a HP+=4 )
set /a NAP-=1 
set Spor=Pozdravil si se za 4 HP!
goto eBitka

:InvPogled
cls
echo.
echo ===================
echo      Inventura
echo ===================                  
echo    Napitek:%NAP%
echo    Car:%CAR%
echo    Olj. venec:%OVN%
echo ===================
echo      1.(nazaj)   
choice /c 1 /n >nul
if %ERRORLEVEL%==1 goto carIn
pause>nul
goto Inventura

:Zmaga
cls
set Spor=Premagal si %eNAME%, cestitke!
set /a LOT+=%eLOT%
echo.
echo ===================
echo       ZMAGA!
echo ===================                  
echo   Nagrada:+%eLOT%
echo   St. zmag:%MK%
echo   Zivljenje:%HP%
echo ===================
echo :%Spor%
pause >nul
goto carIn

:Zguba
cls
set Spor=Zgubil si in komaj pobegnil!
set /a LOT-=%LOT%
if %LOT% LEQ 0 set /a LOT=0
set /a HP=0
set /a MK-=%MK%
if %MK% LEQ 0 set /a MK=0
echo.
echo ===================
echo       ZGUBA!
echo ===================                  
echo   Premozenje:-%LOT%
echo   St. zmag:-%MK%
echo   Zivljenje:%HP%
echo ===================
echo :%Spor%
pause >nul
goto carIn
