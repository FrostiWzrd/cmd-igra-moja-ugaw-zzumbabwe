@echo off

:Menu
cls

type Naslov.txt
echo .
echo 1. Start
echo 2. Credits
echo 3. Exit
set /p answer=Type the number of your option and press enter : 
if %answer%==1 goto Start_1
if %answer%==2 goto Credits
if %answer%==3 goto Exit

:portal
cls
call %answer%
pause>nul

:Exit
cls
echo Thanks for playing!
pause
exit /b

:Start_1
cls
echo You are a adventurer who left his quiet hometown in search of the legendary tower of which many legends spoke that it holds the ultimate power and contains all knowledge hidden in the top floor. You have been roaming the world for two years, gaining experience with your sword and gaining knowledge. One day you were on a quest for an old man who gave you in return an acient book called the NECRONOMICON. The book speaks about an acient hidden tower and its whereabouts so you quickly realise it is speaking about the legendary tower of great power and knowledge. after a long journey you have finally reached the tower.

set /p answer=Press 1 to continue :
if %answer%==1 goto :Continue

:Continue
cls
echo You are now in the tower and see a stairway leading upward and down. Its too dark in here so you have to use a torch. You brought one with you so you should light it. Open your inventory by pressing i.

type Stopnisce.txt
echo .
echo Option 1: Go up the stairs 
echo Option 2: Go down the stairs

set /p answer=

if exist %answer% goto :portal
if %answer%==Up goto :Up
if %answer%==Down goto :Down 

pause

:Up

echo to je zgornje stopnisce 

pause

:Down

echo to je spodnje stopnisce 

pause